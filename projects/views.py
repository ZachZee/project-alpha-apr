from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def show_project(request):
    tasks = Task.objects.all()
    context = {
        "show_project": tasks,
    }
    return render(request, "projects/new_template.html", context)
